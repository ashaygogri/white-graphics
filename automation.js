let fileSystem = require("fs");
let http = require("http");

//fs.writeFile(path_of_the_file, content, callback function)
var contentHTML= "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'><title>Document</title>";
contentHTML+="</head><body></body>";
contentHTML += "</html>";

fileSystem.mkdir(__dirname + "/app", function (e) {
    //app folder
    fileSystem.mkdir(__dirname + "/app/assets", function (e) {
            console.log("assets created");
    });
    
    //inside app
    //images folder
    fileSystem.mkdir(__dirname + "/app/assets/images", function (e) {
            console.log("images created");
    });

//javascript folder
    fileSystem.mkdir(__dirname + "/app/assets/javascript", function (e) {
        fileSystem.writeFile(__dirname + "/app/assets/javascript/jquery.js", "", function (e) {
            console.log("jquery.js created");
        });
        fileSystem.writeFile(__dirname + "/app/assets/javascript/scripts.js", "", function (e) {
            console.log("scripts.js created");
        });

    });
//styles folder
    fileSystem.mkdir(__dirname + "/app/assets/styles", function (e) {
        fileSystem.writeFile(__dirname + "/app/assets/styles/style.css", "", function (e) {
            console.log("styles created");
        });
    });
//inside style
//base css folder
    fileSystem.mkdir(__dirname + "/app/assets/styles/base", function (e) {
        fileSystem.writeFile(__dirname + "/app/assets/styles/base/_global.css", "", function (e) {
            console.log("styles/base created");
        });
        fileSystem.writeFile(__dirname + "/app/assets/styles/base/_mixins.css", "", function (e) {
            console.log("styles/mixins created");
        });
    });
//modules folder
    fileSystem.mkdir(__dirname + "/app/assets/styles/modules", function (e) {
            console.log("styles/modules created");
    });
    
//vendors folder
    fileSystem.mkdir(__dirname + "/app/assets/vendors", function (e) {
            console.log("vendors created");
    });
    fileSystem.mkdir(__dirname + "/app/assets/vendors/animate", function (e) {
            console.log("vendors/animate created");
    });
    
    fileSystem.mkdir(__dirname + "/app/assets/vendors/font-awesome-4.7.0", function (e) {
            console.log("vendors/font-awesome created");
    });
    
    fileSystem.mkdir(__dirname + "/app/assets/vendors/wow", function (e) {
            console.log("vendors/wow created");
    });
    

//main css folder

    fileSystem.mkdir(__dirname + "/app/css", function (e) {
        fileSystem.writeFile(__dirname + "/app/css/style.css", "", function (e) {
            console.log("vendors/wow created");
        });
    });

//index.html 

    fileSystem.writeFile(__dirname + "/app/index.html", contentHTML, function (e) {
        console.log("index.html created");
    });
});

