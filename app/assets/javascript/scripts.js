new WOW().init();


$(document).ready(function(){
    $("#owl").owlCarousel({
        items:3,
        autoplaytrue: true,
        margin: 20,
        loop: true,
        nav: false,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: true,
    });
});

$(document).ready(function(){
    $("#testimonial-owl").owlCarousel({
        items:1,
        autoplaytrue: true,
        margin: 20,
        loop: true,
        nav: false,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: true,
    });
});

$(document).ready(function(){
    $("#price-owl").owlCarousel({
        items:3,
        margin: 20,
//        loop: true,
        nav: false,
        smartSpeed: 700,
        autoplayHoverPause: true,
        dots: true,
    });
});


$(function () {
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
});


$(document).ready(function () {
    $("#client-names").owlCarousel({
        items: 6,
        autoplay: true,
        margin: 20,
        loop: true,
        smartSpeed: 700,
        autoplayHoverPause: true,
    });
});